package com.beautifulhao;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author BeautifulHao
 * @description
 * @create 2019-05-07 14:17
 **/
@RestController
public class TestController {

    @RequestMapping("/hello")
    public String hello(String name) {
        return "hi " + name;
    }

    @RequestMapping("/timeout")
    public String timeout(String name) {
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "timeout params:" + name;
    }

    @RequestMapping("/some-error")
    public String error() {
        throw new UserException();
    }

    @ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "FORBIDDEN")
    public class UserException extends RuntimeException {
        private static final long serialVersionUID = 1L;
    }
}
